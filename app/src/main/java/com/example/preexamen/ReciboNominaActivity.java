package com.example.preexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import androidx.appcompat.app.AppCompatActivity;
import java.util.Random;

public class ReciboNominaActivity extends AppCompatActivity {

    private EditText etNumRecibo, etNombre, etHorasTrabajadas, etHorasExtras, etSubtotal, etImpuesto, etTotal;
    private RadioGroup rgPuesto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);

        etNumRecibo = findViewById(R.id.et_num_recibo);
        etNombre = findViewById(R.id.et_nombre);
        etHorasTrabajadas = findViewById(R.id.et_horas_trabajadas);
        etHorasExtras = findViewById(R.id.et_horas_extras);
        etSubtotal = findViewById(R.id.et_subtotal);
        etImpuesto = findViewById(R.id.et_impuesto);
        etTotal = findViewById(R.id.et_total);
        rgPuesto = findViewById(R.id.rg_puesto);

        Random random = new Random();
        int randomNum = random.nextInt(10000) + 1;
        etNumRecibo.setText(String.valueOf(randomNum));

        Button btnCalcular = findViewById(R.id.btn_calcular);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularNomina();
            }
        });

        Button btnLimpiar = findViewById(R.id.btn_limpiar);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        Button btnRegresar = findViewById(R.id.btn_regresar);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularNomina() {
        int numRecibo = Integer.parseInt(etNumRecibo.getText().toString());
        String nombre = etNombre.getText().toString();
        int horasTrabajadas = Integer.parseInt(etHorasTrabajadas.getText().toString());
        int horasExtras = Integer.parseInt(etHorasExtras.getText().toString());
        int puesto = getPuestoSeleccionado();
        double porcentajeImpuesto = 0.16;

        ReciboNomina recibo = new ReciboNomina(numRecibo, nombre, horasTrabajadas, horasExtras, puesto, porcentajeImpuesto);

        double subtotal = recibo.calcularSubtotal();
        double impuesto = recibo.calcularImpuesto();
        double total = recibo.calcularTotal();

        etSubtotal.setText(String.valueOf(subtotal));
        etImpuesto.setText(String.valueOf(impuesto));
        etTotal.setText(String.valueOf(total));
    }

    private int getPuestoSeleccionado() {
        int selectedId = rgPuesto.getCheckedRadioButtonId();
        if (selectedId == R.id.rb_auxiliar) {
            return 1;
        } else if (selectedId == R.id.rb_albanil) {
            return 2;
        } else if (selectedId == R.id.rb_ing_obra) {
            return 3;
        } else {
            return 1;
        }
    }

    private void limpiarCampos() {
        etNumRecibo.setText("");
        etNombre.setText("");
        etHorasTrabajadas.setText("");
        etHorasExtras.setText("");
        etSubtotal.setText("");
        etImpuesto.setText("");
        etTotal.setText("");
        rgPuesto.check(R.id.rb_auxiliar);

        Random random = new Random();
        int randomNum = random.nextInt(10000) + 1;
        etNumRecibo.setText(String.valueOf(randomNum));
    }
}
